from django.shortcuts           import render
from django.contrib.auth.models import User, Group
from rest_framework             import viewsets, permissions
from .serializers               import UserSerializer, GroupSerializer, AddressSerializer, CompanySerializer, SchoolSerializer, BackgroundSerializer, CourseSerializer, ProjectSerializer, SkillSerializer
from .models                    import Address, Company, School, Background, Course, Project, Skill

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset           = User.objects.all().order_by('-date_joined')
    serializer_class   = UserSerializer
    permission_classes = [permissions.IsAuthenticated]


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset           = Group.objects.all()
    serializer_class   = GroupSerializer
    permission_classes = [permissions.IsAuthenticated]


class AddressViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows addresses to be viewed or edited.
    """
    queryset         = Address.objects.all()
    serializer_class = AddressSerializer


class CompanyViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows companies to be viewed or edited.
    """
    queryset         = Company.objects.all()
    serializer_class = CompanySerializer


class SchoolViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows schools to be viewed or edited.
    """
    queryset         = School.objects.all()
    serializer_class = SchoolSerializer


class BackgroundViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows backgrounds to be viewed or edited.
    """
    queryset         = Background.objects.all()
    serializer_class = BackgroundSerializer


class CourseViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows courses to be viewed or edited.
    """
    queryset         = Course.objects.all()
    serializer_class = CourseSerializer


class ProjectViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows projects to be viewed or edited.
    """
    queryset         = Project.objects.all()
    serializer_class = ProjectSerializer


class SkillViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows skills to be viewed or edited.
    """
    queryset         = Skill.objects.all()
    serializer_class = SkillSerializer