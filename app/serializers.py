from django.contrib.auth.models import User, Group
from rest_framework             import serializers
from .models                    import Address, Company, School, Background, Course, Project, Skill


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model  = User
        fields = ['url', 'username', 'email', 'groups']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model  = Group
        fields = ['url', 'name']


class AddressSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model  = Address
        fields = ['number', 'name', 'city', 'department', 'region', 'country']


class CompanySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model  = Company
        fields = ['logo', 'name', 'address', 'legalForm', 'url', 'description']


class SchoolSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model  = School
        fields = ['logo', 'name', 'address', 'url', 'description']


class BackgroundSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model  = Background
        fields = ['company', 'positionHeld', 'contract', 'start_date', 'end_date', 'description']


class CourseSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model  = Course
        fields = ['school', 'diploma', 'start_date', 'end_date', 'url', 'description']


class ProjectSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model  = Project
        fields = ['overview', 'type', 'name', 'ai_language', 'api_language', 'backend_language', 'desktop_language', 'frontend_language', 'mobile_language', 'script_language', 'ai_framework', 'api_framework', 'backend_framework', 'desktop_framework', 'frontend_framework', 'mobile_framework', 'script_framework', 'url', 'description']


class SkillSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model  = Skill
        fields = ['name', 'type', 'percentage']