from django.db import models

class Address(models.Model):
    class Meta:
        verbose_name        = "Adresse"
        verbose_name_plural = "Adresse"

    number     = models.IntegerField(verbose_name="Num\xe9ro de rue")
    name       = models.CharField   (verbose_name="Nom de rue",     max_length=255)
    city       = models.CharField   (verbose_name="Ville",          max_length=255)
    department = models.CharField   (verbose_name="D\xe9partement", max_length=255)
    region     = models.CharField   (verbose_name="R\xe9gion",      max_length=255)
    country    = models.CharField   (verbose_name="Pays",           max_length=255)

    def __str__(self):
        return str(self.number) + " " + self.name + " " + self.city

class Company(models.Model):
    class Meta:
        verbose_name        = "Entreprise"
        verbose_name_plural = "Entreprises"

    logo        = models.ImageField(verbose_name="Logo de l'entreprise", upload_to="images/companies/")
    name        = models.CharField (verbose_name="Nom de l'entreprise",  max_length=255)
    address     = models.ForeignKey(Address,                             related_name="Entreprise", on_delete=models.CASCADE)
    legalForm   = models.CharField (verbose_name="Statut juridique",     max_length=255)
    url         = models.URLField  (verbose_name="Site web de l'entreprise")
    description = models.TextField (verbose_name="Description de l'entreprise")

    def __str__(self):
        return self.name


class School(models.Model):
    class Meta:
        verbose_name        = "École"
        verbose_name_plural = "Écoles"

    logo        = models.ImageField(verbose_name="Logo de l'\xe9cole", upload_to="images/schools/")
    name        = models.CharField (verbose_name="Nom de l'\xe9cole",  max_length=255)
    address     = models.ForeignKey(Address,                           related_name="Ecole", on_delete=models.CASCADE)
    url         = models.URLField  (verbose_name="Site web de l'\xe9cole")
    description = models.TextField (verbose_name="Description de l'\xe9cole")

    def __str__(self):
        return self.name


class Background(models.Model):
    class Meta:
        verbose_name        = "Expérience Professionnelle"
        verbose_name_plural = "Expériences Professionnelles"

    company      = models.ForeignKey(Company, related_name="Entreprise", on_delete=models.CASCADE)
    positionHeld = models.CharField (verbose_name="Poste occup\xe9",     max_length=255)
    contract     = models.CharField (verbose_name="Contract de travail", max_length=255)
    start_date   = models.DateField (verbose_name="D\xebut de l'exp\xe9rience")
    end_date     = models.DateField (verbose_name="D\xebut de l'exp\xe9rience")
    description  = models.TextField (verbose_name="Description du poste")

    def __str__(self):
        return self.company.name + " " + self.positionHeld


class Course(models.Model):
    class Meta:
        verbose_name        = "Formation"
        verbose_name_plural = "Formations"

    school       = models.ForeignKey(School,                         related_name="Ecole", on_delete=models.CASCADE)
    diploma      = models.CharField (verbose_name="Poste occup\xe9", max_length=255)
    start_date   = models.DateField (verbose_name="D\xebut de l'exp\xe9rience")
    end_date     = models.DateField (verbose_name="Fin de la formation")
    url          = models.URLField  (verbose_name="URL de la formation")
    description  = models.TextField (verbose_name="Description du poste")

    def __str__(self):
        return self.company.name + " " + self.positionHeld

class Project(models.Model):
    class Meta:
        verbose_name        = "Projet"
        verbose_name_plural = "Projets"

    PROJECT_CHOICES = (
        ("os",                      "Systeme d'exploitation"),
        ("kernel",                  "Noyau"),
        ("driver",                  "Pilote"),
        ("language",                "Langage de programmation"),
        ("framework",               "Framework"),
        ("cms",                     "CMS"),
        ("cli",                     "Ligne de commande"),
        ("script",                  "Script"),
        ("search_engine",           "Moteur de recherche"),
        ("game",                    "Jeu"),
        ("search_engine",           "Moteur de recherche"),
        ("browser",                 "Navigateur"),
        ("chat",                    "Messagerie instantanée"),
        ("chatbot",                 "Bot de messagerie"),
        ("bot",                     "Bot"),
        ("desktop",                 "Application de bureau"),
        ("web",                     "Application web"),
        ("mobile",                  "Application mobile"),
        ("showacse_website",        "Site vitrine"),
        ("api",                     "API"),
        ("rest_api",                "API Rest"),
        ("graphql_api",             "API GraphQL"),
        ("utility_software",        "Utilitaire"),
        ("frontend",                "Application frontedn"),
        ("gui",                     "Interface graphique"),
        ("artificial_intelligence", "Intelligence artificielle"),
        ("machine_learning",        "Machine learning"),
        ("deep_learning",           "Deep learning"),
    )

    overview           = models.ImageField(verbose_name="Apercu du projet",                  upload_to="images/projects/")
    type               = models.CharField (verbose_name="Type de projet",                    max_length=255, choices=PROJECT_CHOICES)
    name               = models.CharField (verbose_name="Nom du projet",                     max_length=255)
    ai_language        = models.CharField (verbose_name="Langage de programmation AI",       max_length=255, blank=True, null=True)
    api_language       = models.CharField (verbose_name="Langage de programmation API",      max_length=255, blank=True, null=True)
    backend_language   = models.CharField (verbose_name="Langage de programmation backend",  max_length=255, blank=True, null=True)
    desktop_language   = models.CharField (verbose_name="Langage de programmation desktop",  max_length=255, blank=True, null=True)
    frontend_language  = models.CharField (verbose_name="Langage de programmation frontend", max_length=255, blank=True, null=True)
    mobile_language    = models.CharField (verbose_name="Langage de programmation mobile",   max_length=255, blank=True, null=True)
    script_language    = models.CharField (verbose_name="Langage de programmation script",   max_length=255, blank=True, null=True)
    ai_framework       = models.CharField (verbose_name="Framework AI",                      max_length=255, blank=True, null=True)
    api_framework      = models.CharField (verbose_name="Framework API",                     max_length=255, blank=True, null=True)
    backend_framework  = models.CharField (verbose_name="Framework backend",                 max_length=255, blank=True, null=True)
    desktop_framework  = models.CharField (verbose_name="Framework desktop",                 max_length=255, blank=True, null=True)
    frontend_framework = models.CharField (verbose_name="Framework frontend",                max_length=255, blank=True, null=True)
    mobile_framework   = models.CharField (verbose_name="Framework mobile",                  max_length=255, blank=True, null=True)
    script_framework   = models.CharField (verbose_name="Framework script",                  max_length=255, blank=True, null=True)
    url                = models.URLField  (verbose_name="URL du projet")
    description        = models.TextField (verbose_name="Description du projet")

    def __str__(self):
        return self.name


class Skill(models.Model):
    class Meta:
        verbose_name        = "Compétence"
        verbose_name_plural = "Compétences"

    SKILL_CHOICES = (
        ("os", "Systeme d'exploitation"),
        ("kernel", "Noyau"),
        ("driver", "Pilote"),
        ("language", "Langage de programmation"),
        ("framework", "Framework"),
        ("cms", "CMS"),
        ("cli", "Ligne de commande"),
        ("script", "Script"),
        ("search_engine", "Moteur de recherche"),
        ("game", "Jeu"),
        ("search_engine", "Moteur de recherche"),
        ("browser", "Navigateur"),
        ("chat", "Messagerie instantanée"),
        ("chatbot", "Bot de messagerie"),
        ("bot", "Bot"),
        ("desktop", "Application de bureau"),
        ("web", "Application web"),
        ("mobile", "Application mobile"),
        ("showacse_website", "Site vitrine"),
        ("api", "API"),
        ("rest_api", "API Rest"),
        ("graphql_api", "API GraphQL"),
        ("utility_software", "Utilitaire"),
        ("frontend", "Application frontedn"),
        ("gui", "Interface graphique"),
        ("artificial_intelligence", "Intelligence artificielle"),
        ("machine_learning", "Machine learning"),
        ("deep_learning", "Deep learning"),
    )

    name       = models.CharField   (verbose_name="Nom de la comp\xe9tence", max_length=255)
    type       = models.CharField   (verbose_name="Type de comp\xe9tence",   max_length=255, choices=SKILL_CHOICES)
    percentage = models.IntegerField(verbose_name="Niveau de connaissance")

    def __str__(self):
        return self.name + " " + self.type + " " + str(self.percentage)